#######################
Merge videos together
#######################

This page will describe how the VLC Media Player can be used to Merge video files. 

When merging files, there can one out of the following three situations: 

1. The audio and video codecs of the files to be merged are compatible with each other. Further, the codecs are also compatible with the container (muxer) that we intend to use for the final file.

	Eg 1: You want to merge ``sample1.mp4`` and ``sample2.mp4``, and want the output file to be in an MP4 container. 

2. The audio and video codecs of the original files are not compatible with each other.
	
	Eg 2: You want to merge ``sample1.mp4`` (h265 + mp4a) and ``sample2.ogg`` (theo + vorb).

3. The audio and video codecs of the original files are compatible with each other, but we want the merged file to be contained in a different container. 
	
	Eg 3: You want to merge ``sample1.mp4`` and ``sample2.mp4``, and want the output to be encoded by ``theo`` and ``vorb`` codecs and contained in an ``OGG`` conainer.

You can find which codecs have been used to encode a file by playing it on VLC and navigating to :menuselection:`Tools -> Codec Information`. It will show something like below: 

.. figure::  /images/advanced/transcode/merge_videos_codec_info.png
   :align:   center

   Codec Information for a sample file. 


We will look at all the three cases one by one. 

As a pre-requisite, open the terminal and navigate to the directory where the relevant files are present (otherwise, you will need to specify the ``filepath`` along with ``filename`` in the commands discussed below). 

The commands below are written as they would be used in a Linux based system, you can refer to `How to use VLC through CLI depending on the OS <https://wiki.videolan.org/Command-line_interface/>`_ for altering them to work for Windows and macOS.  


*****************************************
Case - I: All the codecs are compatible
*****************************************

As the codecs are compatible with each other, and with the muxer of the output file, we can simply merge by using ``gather`` in the ``sout`` chain. Hence, we can merge ``sample1.mp4`` and ``sample2.mp4`` by running the following code:

.. code-block::

    $ vlc sample1.mp4 sample2.mp4 --sout="#gather:std{access=file, mux=ts, dst=final.mp4}" --sout-keep


In some cases, the ``sout-apend-file`` option can be used to append files after one another. This can only be used when the original files use compatible codecs, and it usually works for audio files. The code to append ``sample2.mp3`` behind ``sample1.mp3`` is as follows:

.. code-block::

    $ vlc sample1.mp3 sample2.mp3 --sout-append-file --sout="#std{access=file, mux=ts, dst=final.mp3}"

*********************************************************
Case - II: Codecs of original files are not compatible
*********************************************************

As the original files use different codecs, we will have to first transcode all the files so that the codecs are compatible with the muxer that we intend to use for the final file. To understand more about transcoding, you can refer to :ref:`Introduction to Transcoding <introduction-to-transcoding>` and :ref:`How to Transcode a file <transcode-and-save>`.

Transcoding can be done by two methods:

1. Using the Wizard.
2. Using the CLI.

=================
Using the Wizard
=================

To transcode multiple videos using the wizard, follow these steps:

1. Open VLC and go to :menuselection:`Media -> Convert/Save...` (Ctrl+R).
2. Click on the :guilabel:`Add` button and select all the videos you wish to merge. 
3. Click on the :guilabel:`Open` button.
4. Click on the :guilabel:`Click / Save` button.
5. From the dropdown in front of ``Profiles``, select the container format (along with the audio and video codecs) that you wish to transcode your files to.
6. Check the :guilabel:`Append '-converted' to the file name` box.
7. Click on the :guilabel:`Start` button.

.. figure::  /images/advanced/transcode/merge_videos_convert_multiple.png
   :align:   center

   Converting multiple files simultaneously using the Wizard. 


Once all the files are transcoded to the same format, use the first ``sout`` chain from ``Case - I`` to merge the videos.


========================
Using the Command Line
========================

To transcode and merge the videos using the command line, simply add the ``transcode`` option in front of ``gather`` in the ``sout`` chain.

Following is the code to merge ``sample1.mp4`` and ``sample2.ogg`` and get the final result is an MP4 container: 

.. code-block::

    $ vlc sample1.mp4 sample2.ogg --sout="#transcode{vcodec=h265, acodec=mp4a}:gather:std{access=file, mux=ts, dst=final.mp4}" --sout-keep


********************************************************************************************************************
Case - III: Codecs of the original files are compatible, but need the output to be contained in a different format
********************************************************************************************************************

This case is a mixture of the above two cases. As the original files use compatible codecs, we can first simply merge them using the ``sout`` chain mentioned in ``Case - I``. Then, the merged file can be transcoded to the desired format by using the Wizard, as mentioned in ``Case - II``.

Another way to achieve the same result is to simply re-encode the original files using the ``transcode`` block in the ``sout`` chain, and then to merge all the transcoded files using ``gather`` and ``std`` blocks. 

The code to merge ``sample1.mp4`` and ``sample2.mp4`` and store the final output in an OGG container would look something like the following:

.. code-block::

	vlc sample1.mp4 sample2.mp4 --sout="#transcode{vcodec=theo, acodec=vorb}:gather:std{access=file, mux=ogg, dst=final.ogg}" --sout-keep
