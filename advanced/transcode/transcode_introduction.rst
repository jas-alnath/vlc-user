.. _introduction-to-transcoding:

################################
Introduction to Transcoding
################################

This page describes what transcoding is, explains how it works, and describes various scenarios where transcoding is useful. 


*******************************************
What is Transcoding
*******************************************

Transcoding is the process of decoding a media file and re-encoding it. Sometimes, the re-encoding is done while using completely different codecs as compared to the original file. At other times, we re-encode to change a specific property (like the bitrate) or add some filter/effect over the media file.


.. _combine-transcode-and-std-output:

******************************************
Combining Transcoding and Standard Output
******************************************

Transcoding is usually directly or indirectly followed by the ``standard`` module, for either saving the newly created file locally or streaming it over the network. This is because after the ``transcode`` module performs the required modifications, we need the ``std`` module to answer "where to send the new content?"

The basic idea behind transcoding is to start from the raw audio, video, and subtitle streams of a media file and follow this process:

 + The raw streams are encoded using the relevant ``codecs`` (using ``acodec``, ``vcodec``, ``scodec`` of the ``transcode`` module). 
 + The encoded streams are then passed over to a ``muxer`` which combines all the streams to make a single muxed stream. (The muxer is usually specified in the ``mux`` parameter of the ``std`` module).
 + The muxed stream is then either saved or streamed over the network (using the ``access`` parameter of the ``std`` module).

The following figure summarizes the process:

.. graphviz::

	digraph screen_record {
	rankdir=LR;
	node [shape=box];

	{ node [width=0 shape=point label=""]; video, audio, sub, v, s, out;
	}
	
	subgraph cluster_one {
	    label="codecs"
	    "vcodec", "acodec", "scodec"
	}

	video -> "vcodec" [label="Raw video\nstream"]
	"vcodec" -> v [arrowhead=none] [label="Encoded\nvideo stream"]
	v -> "mux" 

	audio -> "acodec" [label="Raw audio\nstream"]
	"acodec" -> "mux" [label="Encoded\naudio stream"]


	sub -> "scodec" [label="Raw subtitle\nstream"]
	"scodec" -> s [arrowhead=none] [label="Encoded\nsubtitle stream"]
	"mux" -> s [dir=back]

	"mux" -> "access" [label="Muxed stream"]

	"access" -> out [label="Muxed stream\ntransported\nby access"]

	subgraph text {
	rank=same;
	v, "mux", s
	}
	
	}

|

Using a correct combination of codecs and a muxer is the key part of successful transcoding. It is advised to refer to the Compatibility Matrix to make sure that the codecs and the muxer being used for a transcoding operation are compatible.

**************************************************************************
Scenarios arising from the combination of Transcode and Standard Output
**************************************************************************

When we combine the ``transcode`` and the ``standard`` modules, there can be the following four scenarios:

=======================  ================================  ===================================
Conversion/Output        File                              Stream
=======================  ================================  ===================================
No transcode             Remuxing a media file             Streaming content ``as is``
Transcode                Saving content after conversion   Streaming content after conversion
=======================  ================================  ===================================  


.. _scenario-one:

+++++++++++++++++++++++++++++++++++++++++++
Scenario 1: No conversion + Save the file
+++++++++++++++++++++++++++++++++++++++++++

Remuxing a file from one format to another is a common use case for this scenario. Remuxing is the process in which we take the encoded audio, video, and subtitle streams, and ``mux`` them into a new container. 

Unlike in transcoding, we don't need to re-encode the streams when we are remuxing. This is why remuxing is much faster than transcoding. 


.. _scenario-two:

+++++++++++++++++++++++++++++++++++++++++++++++++++++
Scenario 2: No conversion + Stream over the network
+++++++++++++++++++++++++++++++++++++++++++++++++++++

The most common use case of this scenario is to stream a file over the network without modifying it. VLC supports streaming over various protocols like HTTP, UDP, RTP, and RTSP. Refer to the Stream over the Network section for more details. 

Note that though it is possible to stream over the internet, "Streaming over the network" here refers to streaming over the local network. Streaming over the local network means that the streaming and the receiving device(s) should be connected to the same router.

Another use case of this scenario is to proxify a stream, in which we can change the protocol of the destination. For example, opening a UDP stream and proxifying it to send an HTTP stream as the output.

Internally, VLC uses this feature for chromecast. Chromecast expects an HTTP stream, so everything works normally when VLC sends an HTTP stream to the chromecast. However, we can use VLC to send an RTSP stream to the chromecast as well - VLC will simply proxify the stream to HTTP.


.. _scenario-three:

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Scenario 3: Convert to a different format + Save the file
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

This scenario is the basic transcoding scenario. There are various reasons why someone might wish to transcode a media file and re-encode the streams using different codecs or settings. Some reasons can be:

 + A different codec might offer a better compression and reduce the file size significantly.
 + Some codecs might offer higher customization or they might be supported across more platforms.
 + You may wish to burn the subtitles on the video.
 + You might want to apply various video, audio, or a subtitle filters, etc. 

Therefore, the common use cases that fall in this scenario are:

 + :ref:`Transcode a file and save the new file locally <transcode-and-save>`. 
 + :ref:`Record the screen and save it in a new file locally <screen-record-and-save>`. 
 + Add an audio or video filter over the media file, and save the new file locally.


.. _scenario-four:

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Scenario 4: Convert to a different format + Stream over the network
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

This scenario involves more variables to think about, as it combines the ``simple streaming`` aspect of scenario two with the ``transcoding`` aspect of scenrio three. However, it is easy to implement its use cases when we think of the ``sout chain`` as a pipeline.

The use cases of this scenario include: 

 + :ref:`Transcode a file and stream it over the local network <transcode-and-stream>`.
 + :ref:`Record the screen and stream the playback over the local network <screen-record-and-stream>`. 
 + Add an audio or video filter over a media file, and stream it over the local network.

In all the above cases, the basic approach is to first implement the transcoding part as done in the corresponding use cases in scenario three. After that, we simply stream the ``muxed stream`` over the local network like we would stream a normal file. 
