.. _vlm-introduction:

####################
VideoLAN Manager
####################

This page gives a basic introduction to what the VideoLAN Manager (VLM) is and how to use it to broadcast a simple stream.


******************
What is the VLM?
******************

VLM is a media manager which can be used to launch and control multiple streams simultaneously. Apart from using the VLM for controlling streams from a single instance of VLC, we can also use it for scripting a set of commands in a ``.vlm`` file which can be executed by a simple command. 

Using the ``.vlm`` file makes it convenient to repeat the same operation multiple times without having to go into the details of the process everytime.

VLM can be used for doing three kinds of operations:

 + Broadcast
 + Schedule
 + Video on Demand (VoD)

In this page, we will create a ``.vlm`` file for a simple broadcast.


.. _vlm-single-stream:

*************************************************
How to broadcast a single stream using the VLM?
*************************************************

For this example, we will stream a file ``sample.mp3`` over UDP in the multicasting mode. Therefore, as a pre-requisite, refer to Stream over UDP to understand how VLC can be used to stream over UDP.


++++++++++++++++++++++++++++++
Creating the VLM file.
++++++++++++++++++++++++++++++

To write the code along, open a new file in any text editor. 

First, we will declare a new VLM media element using the ``new`` keyword. We need to specify whether it will be broadcast, schedule, or vod. As we are doing a broadcast, we will write that like in the following line: 

.. code-block ::

   new channel_one broadcast enabled

Here, ``channel_one`` is the name of the vlm element. This name will be used to refer to it in subsequent commands. 

Now, we will specify the file (along with its path) that we wish to broadcast. Don't forget to modify the file path according to where the file is located in your computer:

.. code-block ::

   setup channel_one input "file:///C:/Users/User_name/Desktop/sample.mp3"

Be careful to use slash (and not backslash) while writing the address, as that is the appropriate syntax for `file URI addresses <https://en.wikipedia.org/wiki/File_URI_scheme>`_.

Next, we will specify the stream output. For this example, we will stream at the multicast IP address ``239.255.255.250`` and port ``8090``. We will also make SAP announcement along with it so that it's more convenient for the receiver to connect.

.. code-block ::

   setup channel_one output #udp{mux=ts,dst=239.255.255.250:8090,sap,name=testing}

In the above line of code, we have named our SAP announcement ``testing`` (you can use any name as you wish). This will be the name of the stream that the receiving systems will connect to. 

As a final step, we will tell the VLM to play this element by the following code: 

.. code-block ::

   control channel_one play

In the end, the file must be looking something like this:

.. code-block ::

   new channel_one broadcast enabled
   setup channel_one input "file:///C:/Users/User_name/Desktop/sample.mp3"
   setup channel_one output #udp{mux=ts,dst=239.255.255.250:8090,sap,name=testing}
   control channel_one play

Save this file with the extension ``.vlm``. 

+++++++++++++++++++++++++++++
How to start the broadcast?
+++++++++++++++++++++++++++++

Open the terminal and navigate to the directory where you have saved the ``.vlm`` file. Run the following code to execute the commands written in the ``.vlm`` file:

.. code-block ::

   $ vlc --vlm-conf=example.vlm

Alternatively, you can also run the ``.vlm`` file from the GUI Wizard, as discussed later in the page. 

+++++++++++++++++++++++++++++++
How to receive the broadcast?
+++++++++++++++++++++++++++++++

This broadcast can be received on any device connected to the same local network. It can be done by following these steps: 

 + On the receiving system, open the VLC media player and select to :menuselection:`View -> Playlist`. 
 + Click on ``Network Streams (SAP)`` which is in the ``Local Network`` section on the left side.
 + You will soon see a SAP stream by the name ``testing`` (like in the figure below).
 + Double click on it to connect and start receiving.  

.. figure::  /images/advanced/vlm/introduction_SAP.png
   :align:   center

   How the stream looks like in the VLC window.

|


.. vlm-wizard:

***************************************
Configuring the VLM through the Wizard
***************************************

VLC provides an alternate method to configure the VLM for most of the simple use cases. You can open the VLC media player and navigate to :menuselection:`Tools -> VLM Configuration` to configure the VLM from the wizard. 

.. figure::  /images/advanced/vlm/introduction_vlm_wizard.png
   :align:   center
   :scale: 70 %

   VLM Configuration from the Wizard.

As you will notice, you can configure the VLM for all three modes. After setting the name, input, and output parameters, you can ``Add`` the element to the :menuselection:`Media Manager List` and play them. 

If you wish to save those parameters, you can use the :guilabel:`Export` button to export them to a ``.vlm`` file. 

Similarly, you can use the :guilabel:`Import` button to open an existing ``.vlm`` file and run it.

*********************************************************
What are some other things for which VLM can be used?
*********************************************************

VLM can be used for scripting and automating various scenarios, for example:

 + Scheduling a stream.
 + :ref:`Broadcast multiple streams <vlm-broadcast-multiple>`. 
 + :ref:`Broadcast a playlist <vlm-playlist>`.
 + :ref:`Mosaic broadcasting <vlm-mosaic>`.
 + :ref:`Share your screen in real time <vlm-share-screen>`.


.. _uri-and-path-syntax:

**********************************************
Alternate Syntax for the input file address
**********************************************

Instead of using the file-URI syntax for specifying the location of the input file, you can also use the syntax used by the `PATH variabes <https://en.wikipedia.org/wiki/Path_(computing)>`_ of your OS. 

Hence, for Windows, the URI ``file:///C:/Users/User_name/Desktop/sample.mp3`` is equivalent to the path ``C:\Users\User_name\Desktop\sample.mp3``. Notice that the directory separator is ``/`` in the file-URI syntax but ``\`` in the PATH syntax. 

While in Linux, the URI ``file:///home/user_name/Desktop/sample.mp3`` is equivalent to the path ``/home/user_name/Desktop/sample.mp3``. In this case, the directory separator is ``/`` in both the cases.
