.. _stream-with-sap:

################################
Using SAP while streaming
################################

SAP stands for Session Announcement Protocol. It provides a way for the receiving systems to catch a multicast stream without manually setting the correct IP address and port number.

As any system in the local network can receive announcements from all others without any manual configuration, SAP proves to be an easy-to-use mechanism in cases where multiple clients are receiving the stream. 

For example, a university can make SAP announcements for broadcasting various TV channels over their student network. All the students will be able to: 

 + View the list of all the channels being broadcasted.
 + Connect to a channel of their choice with just a single click.

It is important to note that SAP announcements can only be sent in the local network, which means that the streaming device and all the receiving device(s) should be connected to the same router for the streaming to work. 

We can use SAP when we are streaming over UDP or RTP. Hence, as a prerequisite, refer to :ref:`Stream over UDP <stream-over-udp>` or :ref:`Stream over RTP <stream-over-rtp>` to understand how streaming over those protocols works.

.. _make-sap-announcement:

*********************************
Making a basic SAP announcement 
*********************************

As an example, let's assume that the network IP addresses of a few of the devices in the local network are:

 + Streaming device: 192.168.0.101
 + Receiving device(s): 192.168.0.102, 192.168.0.103, 192.168.0.104
 + Multicast address: 239.255.255.250

.. graphviz::

	digraph udp_network {
	  rankdir=TB;
	  node [shape=circle];
	  
	  { node [shape=plaintext] "Multicast IP address: 239.255.255.250"
	  }

	  { node [shape=box]; "\n Router \n 192.168.0.1 \n\n"
	  }

	 "\n Router \n 192.168.0.1 \n\n" -> "Device 2 \n 192.168.0.102"  [arrowhead=none]
	 "\n Router \n 192.168.0.1 \n\n" -> "Device 3 \n 192.168.0.103"  [arrowhead=none]
	 "Device 2 \n 192.168.0.102" -> "Multicast IP address: 239.255.255.250" [style=invis]
	 "Device 3 \n 192.168.0.103" -> "Multicast IP address: 239.255.255.250" [style=invis]


	  subgraph devices {
	  rank=same;
	  rankdir=LR;
	  "Device 1 \n 192.168.0.101" -> "\n Router \n 192.168.0.1 \n\n" -> "Device 4 \n 192.168.0.104" [arrowhead=none] [label="                                 "];
	  }

	}

|

To start a SAP session while streaming using UDP or RTP, simply add the following parameters in the ``std`` block of the ``sout`` chain:

 + The keyword ``sap``, and
 + Set the ``name`` parameter equal to the desired name for the stream.

Hence, the code would look like the following when making a SAP anouncement while multicast streaming over UDP:

.. code-block ::

   $ vlc sample.mp4 --sout="#std{access=udp, mux=ts, sap, name=Test_stream, dst=239.255.255.250:8090}"

Similarly, the code to make SAP announcement when streaming over RTP would look like: 

.. code-block ::

   $ vlc sample.mp4 --sout="#rtp{mux=ts, sap, name=Test_stream, dst=239.255.255.250, port=5010}"



.. _connect-to-sap-stream:

*********************************
Connecting to a SAP stream
*********************************

To connect to the SAP stream, follow these steps on the receiving device(s):

 + Open VLC Media Player.
 + Go to :menuselection:`View -> Playlist` (Shortcut Ctrl + L).
 + Navigate to ``Local Networks`` on the left side.
 + Click on :guilabel:`Network streams (SAP)`.
 + The player will look for SAP announcements and list all the currently available streams.
 + Double click on the stream you wish to start receiving from.

.. figure::  /images/advanced/streaming/sap_session_receive.png
   :align:   center

As soon as you double click on the SAP announcement (``Test_stream`` in the above figure), VLC will start playing the media file.


**********************************
How does streaming with SAP work?
**********************************

SAP is a transport protocol. It uses SDP (Session Description Protocol) descriptions for "describing" the UDP/RTP stream over which the media file is being streamed. 

The sender periodically sends these SDP descriptions to the multicast IP address ``224.2.127.254`` at port number ``9875``. The SDP messages contain meta-data that convey sufficient information about the stream for the client(s) to discover and connect to it (including name of the stream, audio/video codecs, IP address and the port number at which the UDP/RTP stream is being pushed, etc.)
 
Any device connected to the local network can use the ``SAP Service Discovery Plugin`` of VLC to listen for these announcements. The plugin simply scans ``224.2.127.254:9875`` and lists all the available streams that the client device can connect to. 

Once the user decides to connect to a particular stream, VLC passes the relevant information from the SDP description to the ``udp`` or ``rtp`` module. Then the client device connects to the UDP/RTP stream and starts receiving the stream. 

Hence, we can see that two protocols are involved in this entire process - SAP (over which SDP descriptions are sent), and UDP/RTP (over which the file is being streamed).