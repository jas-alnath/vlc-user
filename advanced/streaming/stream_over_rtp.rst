.. _stream-over-rtp:

################################
Stream over RTP
################################

This page explains how VLC Media Player can be used to stream a file locally over RTP. Streaming locally means that the streaming device and all the receiving device(s) should be connected to the same router for the streaming to work.

To stream over RTP, we will use the ``rtp`` module in the ``sout`` chain. Note that unlike when streaming over HTTP or UDP, we cannot use the ``std{access=`` format for streaming over RTP.

For VLC, streaming over RTP works by pushing the content to the target IP, which can be:

 + A unicast address (when streaming to a specific device), or
 + A multicast address (when streaming to multiple devices in the local network).

The default port suggested for RTP streaming is ``5004``, however you can pick any of the free ports as per your choice. For this example we will stream on port number ``5010``.


.. _unicast-stream-over-rtp:

****************************
Unicast Streaming over RTP
****************************

Let us assume that the IP addresses of the server and client are:

 + Streaming device: 192.168.0.101
 + Receiving device: 192.168.0.102


+++++++++++++++++++++++++++++++
Sending the Unicast Stream
+++++++++++++++++++++++++++++++

To push the stream, run the following code on the streaming device: 

.. code-block ::

   $ vlc sample.mp4 --sout="#rtp{mux=ts, dst=192.168.0.102, port=5010}"

Notice that the IP address in ``dst`` is the IP address of the receiving device.


+++++++++++++++++++++++++++++++
Receiving the Unicast Stream
+++++++++++++++++++++++++++++++

To receive the stream, run the following code on the receiving device: 

.. code-block ::

   $ vlc rtp://:5010

Alternatively, you could write the IP address of the client device itself which will accomplish the same thing:

.. code-block ::

   $ vlc rtp://192.168.0.101:5010


Diagramatically, this is a simplified representation of how the network would look like when unicast streaming over ``rtp``:


.. graphviz::

	digraph rtp_unicast {
    rankdir=TB;
    node [shape=circle];
    
    { node [width=0 shape=point label=""]; idle;
    }
    
    { node [shape=plaintext]; "$ vlc sample.mp4 --sout=\"#rtp{\nmux=ts, dst=192.168.0.102, port=5010}\"", "$ vlc rtp://:5010";
    }
    
    
    "Streaming Device \n IP: 192.168.0.101" -> "Receiving Device \n IP: 192.168.0.102" [label="The server \"pushes\" the stream to\nthe client's IP at the specified port."] 
    "Streaming Device \n IP: 192.168.0.101":w -> "$ vlc sample.mp4 --sout=\"#rtp{\nmux=ts, dst=192.168.0.102, port=5010}\"":w [style=invis]
    "Receiving Device \n IP: 192.168.0.102" -> "$ vlc rtp://:5010" [style=invis]
    "Idle Device \n IP: 192.168.0.103" -> "Receiving Device \n IP: 192.168.0.102" [style=invis]
    
    idle -> "Streaming Device \n IP: 192.168.0.101" [arrowhead=none]
    idle -> "Idle Device \n IP: 192.168.0.103" [arrowhead=none] [label="This device will not receive the stream because\nthe server didn't push the stream to its IP address."]

   subgraph devices {
    rank=same;
    "Streaming Device \n IP: 192.168.0.101", "Receiving Device \n IP: 192.168.0.102";
    }
    
    subgraph arrow {
    rank=same;
    idle, "Idle Device \n IP: 192.168.0.103";
    }

  }

|


.. _multicast-stream-over-rtp:

****************************
Multicast Streaming over RTP
****************************

Streaming to the multicast IP address essentially means that the stream will the pushed to all the devices in the local network. So any device that wishes to receive the stream can connect to the relevant port and start receiving. 

Continuing with the example above, let's assume that the IP address of the streaming device is the same (that is, ``192.168.0.101``), and that the multicast IP address is ``239.255.255.250``. 


+++++++++++++++++++++++++++++++
Sending the Multicast Stream
+++++++++++++++++++++++++++++++

To push the stream, run the following code in the streaming device: 

.. code-block ::

   $ vlc sample.mp4 --sout="#rtp{mux=ts, dst=239.255.255.250, port=5010}"


+++++++++++++++++++++++++++++++
Receiving the Multicast Stream
+++++++++++++++++++++++++++++++

To receive the stream, run the following code on the receiving devices: 

.. code-block ::

   $ vlc rtp://239.255.255.250:5010

Note that for receiving the multicast stream, it is important to specify the multicast IP address. Diagramatically, following is how the network might look like in this scenario:

.. graphviz::

    digraph rtp_unicast {
      rankdir=TB;
      node [shape=circle];

      { node [width=0 shape=point label=""]; two, one;
      }
      
      { node [shape=plaintext]; "$ vlc sample.mp4 --sout=\"#rtp{\nmux=ts, dst=239.255.255.250, port=5010}\"", "$ vlc rtp://239.255.255.250:5010\nOn the Receiving Devices.";
      }
      
      {node [shape=box] "\n  Network  \n  Switch  \n "
      }
      
      "\n  Network  \n  Switch  \n " -> "Idle Device\n IP: 192.168.0.102" [label="The other devices can\nremain idle, although,\nthe stream will still be\npushed to them in case\nthey wish to connect."] 
      "Streaming Device \n IP: 192.168.0.101":w -> "$ vlc sample.mp4 --sout=\"#rtp{\nmux=ts, dst=239.255.255.250, port=5010}\"":w [style=invis]
      "Idle Device\n IP: 192.168.0.102" -> "$ vlc rtp://239.255.255.250:5010\nOn the Receiving Devices." [style=invis]
      "Receiving Device 2\n IP: 192.168.0.103" -> "Idle Device\n IP: 192.168.0.102" [style=invis]
      "Receiving Device 1\n IP: 192.168.0.107" -> "Receiving Device 2\n IP: 192.168.0.103" [style=invis]
      
      one -> two [arrowhead=none]
      one -> "Receiving Device 1\n IP: 192.168.0.107" [label="The server pushes the stream\nto all the devices in the local\nnetwork at the specified port."]
      two -> "\n  Network  \n  Switch  \n " [arrowhead=none]
      two -> "Receiving Device 2\n IP: 192.168.0.103" [label="The devices that wish to\nreceive the stream can do\nso by connecting to the udp\nstream at that specific port."]

      "Streaming Device \n IP: 192.168.0.101" -> "\n  Network  \n  Switch  \n "

      subgraph devices {
      rank=same;
      "\n  Network  \n  Switch  \n ", "Streaming Device \n IP: 192.168.0.101", "Idle Device\n IP: 192.168.0.102";
      }
      
      subgraph arrow {
      rank=same;
      two, "Receiving Device 2\n IP: 192.168.0.103";
      }

      subgraph arrow2{
        rank=same;
        one, "Receiving Device 1\n IP: 192.168.0.107";
      }

    }

|

*************************
Using Session Protocols
*************************

You can use SAP announcements while streaming over RTP. This will help to "discover" streams on the receiving devices and seamlessly connect without having to execute any code. Refer to :ref:`Using SAP while Streaming <stream-with-sap>` to understand how to use SAP announcements.

You can also use RTSP as the session management protocol. In this, the streaming will still happen over RTP but the session parameters (like IP address and the port number) will be negotiated by the RTSP protocol. Hence, the sender will not have to manually mention the IP address(es) of the receivers. Refer to :ref:`Stream using RTSP <stream-with-rtsp>` to understand how to use RTSP for streaming.

Taking it a step further, it is possible to use both SAP and RTSP simultaneously. This gives the client the choice to either connect through the SAP discovery option, or by using the RTSP link.